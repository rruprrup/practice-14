import Game from './Game.js';
import SpeedRate from './SpeedRate.js';
import Card from './Card.js';


// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    if (isDuck(card) && isDog(card)) {
        return 'Утка-Собака';
    }
    if (isDuck(card)) {
        return 'Утка';
    }
    if (isDog(card)) {
        return 'Собака';
    }
    return 'Существо';
}

class Creature extends Card {
    constructor(name, maxPower) {
        super();
        this.name = name;
        this.maxPower = maxPower;
    }
    getDescriptions() {
        return [
            super.getDescriptions(),
            getCreatureDescription(this)
        ];
    }
}


// // Основа для утки.
class Duck extends Creature {
    constructor(name, maxPower, isDuck) {
        super();
        this.name = "Мирная утка";
        this.maxPower = 2;
        this.isDuck = true;
    }
}

// // Основа для собаки.
class Dog extends Creature {
    constructor(name, maxPower, isDog) {
        super();
        this.name = "Пёс-бандит";
        this.maxPower = 3;
        this.isDog = true;
    }
}


// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
    new Duck(),
    new Duck(),
    new Duck()
];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
    new Dog()
];


// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
SpeedRate.set(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});